<?php

declare(strict_types=1);

namespace App\Entity;

class Product
{
    private int $id;
    private string $categoryName;
    private string $sku;
    private string $name;
    private string $description;
    private string $shortDescription;
    private float $price;
    private string $link;
    private string $image;
    private string $brand;
    private int $rating;
    private string $caffeineType;
    private string $count;
    private string $flavored;
    private string $seasonal;
    private string $instock;
    private int $facebook;
    private int $isKCup;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getCategoryName(): string
    {
        return $this->categoryName;
    }

    public function setCategoryName(string $categoryName): void
    {
        $this->categoryName = $categoryName;
    }

    public function getSku(): string
    {
        return $this->sku;
    }

    public function setSku(string $sku): void
    {
        $this->sku = $sku;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(string $shortDescription): void
    {
        $this->shortDescription = $shortDescription;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    public function getBrand(): string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): void
    {
        $this->brand = $brand;
    }

    public function getRating(): int
    {
        return $this->rating;
    }

    public function setRating(int $rating): void
    {
        $this->rating = $rating;
    }

    public function getCaffeineType(): string
    {
        return $this->caffeineType;
    }

    public function setCaffeineType(string $caffeineType): void
    {
        $this->caffeineType = $caffeineType;
    }

    public function getCount(): string
    {
        return $this->count;
    }

    public function setCount(string $count): void
    {
        $this->count = $count;
    }

    public function getFlavored(): string
    {
        return $this->flavored;
    }

    public function setFlavored(string $flavored): void
    {
        $this->flavored = $flavored;
    }

    public function getSeasonal(): string
    {
        return $this->seasonal;
    }

    public function setSeasonal(string $seasonal): void
    {
        $this->seasonal = $seasonal;
    }

    public function getInstock(): string
    {
        return $this->instock;
    }

    public function setInstock(string $instock): void
    {
        $this->instock = $instock;
    }

    public function getFacebook(): int
    {
        return $this->facebook;
    }

    public function setFacebook(int $facebook): void
    {
        $this->facebook = $facebook;
    }

    public function getIsKCup(): int
    {
        return $this->isKCup;
    }

    public function setIsKCup(int $isKCup): void
    {
        $this->isKCup = $isKCup;
    }
}