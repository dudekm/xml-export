<?php

declare(strict_types=1);

namespace App\Command\File;

interface FileInterface
{
    public function mapping(string $content): array;
}