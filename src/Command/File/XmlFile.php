<?php

declare(strict_types=1);

namespace App\Command\File;

use App\Entity\Product;
use Symfony\Component\Serializer\SerializerInterface;

class XmlFile implements FileInterface
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param string $content
     * @return Product[]
     */
    public function mapping(string $content): array
    {
        $productsData = $this->serializer->decode($content, 'xml');

        return $this->mappedValues($productsData);
    }

    /**
     * @param array $productsData
     * @return Product[]
     */
    private function mappedValues(array $productsData): array
    {
        $products = [];

        if (!empty($productsData['item'])) {
            foreach ($productsData['item'] as $row) {
                $product = new Product();
                $product->setId((int)$row['entity_id']);
                $product->setCategoryName((string)$row['CategoryName']);
                $product->setSku($row['sku']);
                $product->setDescription((string)$row['description']);
                $product->setShortDescription((string)$row['shortdesc']);
                $product->setPrice((float)$row['price']);
                $product->setLink($row['link']);
                $product->setImage($row['image']);
                $product->setBrand((string)$row['Brand']);
                $product->setRating((int)$row['Rating']);
                $product->setCaffeineType((string)$row['CaffeineType']);
                $product->setCount((string)$row['Count']);
                $product->setFlavored((string)$row['Flavored']);
                $product->setSeasonal((string)$row['Seasonal']);
                $product->setInstock($row['Instock']);
                $product->setFacebook((int)$row['Facebook']);
                $product->setIsKCup((int)$row['IsKCup']);

                $products[] = $product;
            }
        }

        return $products;
    }
}
