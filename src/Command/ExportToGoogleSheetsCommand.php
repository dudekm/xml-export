<?php

declare(strict_types=1);

namespace App\Command;

use App\Repository\ProductRepository;
use App\Service\ExportToGoogleSheetsService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExportToGoogleSheetsCommand extends Command
{
    const PATH = 'path';
    const GOOGLE_DRIVE_URL = 'https://docs.google.com/spreadsheets/d/';

    protected static $defaultName = 'app:export-to-google-sheets';

    private ExportToGoogleSheetsService $exportService;
    private ProductRepository $productRepository;

    public function __construct(ExportToGoogleSheetsService $exportService, ProductRepository $productRepository)
    {
        parent::__construct();
        $this->exportService = $exportService;
        $this->productRepository = $productRepository;
    }

    protected function configure(): void
    {
        $this->setName(self::PATH)
            ->setDescription('Export XML file to Google Sheets')
            ->addArgument(self::PATH, InputArgument::REQUIRED, 'Pass a path to the file (local or remote file)');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $pathInput = $input->getArgument(self::PATH);
        $startMessage = sprintf('Path: %s', $pathInput);
        $output->writeln($startMessage);

        $products = $this->productRepository->findAll($pathInput);

        $spreadSheetId = $this->exportService->exportProducts($products);

        $output->writeln('Done. Spreadsheet URL: ' . self::GOOGLE_DRIVE_URL . $spreadSheetId);

        return Command::SUCCESS;
    }
}
