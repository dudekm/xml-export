<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_Permission;
use Google_Service_Sheets_Spreadsheet;
use Google_Service_Sheets_ValueRange;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;

class GoogleSheetRepository extends GoogleSheetsAbstractRepository
{
    private SerializerInterface $serializer;

    public function __construct(Google_Client $client, SerializerInterface $serializer)
    {
        parent::__construct($client);
        $this->serializer = $serializer;
    }

    public function createSpreadSheet(?string $title = 'New spreadsheet', ?string $spreadsheetId = 'spreadsheetId'): string
    {
        $spreadsheet = new Google_Service_Sheets_Spreadsheet([
            'properties' => [
                'title' => $title
            ]
        ]);

        $createdSpreadSheet = $this->serviceSheets->spreadsheets->create($spreadsheet, [
            'fields' => $spreadsheetId
        ]);

        return $createdSpreadSheet->spreadsheetId;
    }

    public function setSpreedSheetPermissions($spreadsheetId): void
    {
        $googleDrive = new Google_Service_Drive($this->client);
        $googleDrivePermission = new Google_Service_Drive_Permission();
        $googleDrivePermission->setType('anyone');
        $googleDrivePermission->setRole('reader');
        $googleDrive->permissions->create($spreadsheetId, $googleDrivePermission);
    }

    /**
     * @param string $spreadsheetId
     * @param Product[] $products
     * @throws ExceptionInterface
     */
    public function saveSpreadSheetValues(string $spreadsheetId, array $products): void
    {
        $productsValues = $this->prepareData($products);
        $valueRange = new Google_Service_Sheets_ValueRange();
        $valueRange->setValues($productsValues);

        $range = 'Sheet1!A1:A';
        $config = ['valueInputOption' => 'USER_ENTERED'];
        $this->serviceSheets->spreadsheets_values->append($spreadsheetId, $range, $valueRange, $config);
    }

    /**
     * @param Product[] $products
     * @return array
     * @throws ExceptionInterface
     */
    private function prepareData(array $products): array
    {
        $productsData = array_values($this->serializer->normalize($products, null));

        $productsValues = [];
        foreach ($productsData as $key => $product) {
            if ($key === 0) {
                $productsValues[] = array_keys($product);
            }
            $productsValues[] = array_values($product);
        }

        return $productsValues;
    }
}
