<?php

declare(strict_types=1);

namespace App\Repository;

abstract class FetchAbstractRepository
{
    protected function read(string $path): string
    {
        return file_get_contents($path);
    }
}