<?php

declare(strict_types=1);

namespace App\Repository;

use App\Command\File;
use App\Entity\Product;
use Symfony\Component\Serializer\SerializerInterface;

class ProductRepository extends FetchAbstractRepository
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param string $path
     * @return Product[]
     */
    public function findAll(string $path): array
    {
        $data = $this->read($path);

        $fileExtension = pathinfo($path)['extension'];
        $file = match ($fileExtension) {
            'xml' => new File\XmlFile($this->serializer),
        };

        return $file->mapping($data);
    }
}
