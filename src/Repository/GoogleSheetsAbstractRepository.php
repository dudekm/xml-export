<?php

declare(strict_types=1);

namespace App\Repository;

use Google_Client;
use Google_Service_Sheets;

abstract class GoogleSheetsAbstractRepository
{
    protected Google_Client $client;
    protected Google_Service_Sheets $serviceSheets;

    public function __construct(Google_Client $client)
    {
        $this->client = $client;
        $this->client->setApplicationName('Export XML file');

        $this->client->setScopes([
            Google_Service_Sheets::SPREADSHEETS,
            Google_Service_Sheets::DRIVE,
        ]);
        $this->client->setAccessType('offline');
        $this->client->setAuthConfig($_ENV['GOOGLE_APP_CREDENTIALS']);

        $this->serviceSheets = new Google_Service_Sheets($this->client);
    }
}