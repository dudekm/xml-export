<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Product;
use App\Repository\GoogleSheetRepository;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class ExportToGoogleSheetsService
{
    private GoogleSheetRepository $googleSheetRepository;

    public function __construct(GoogleSheetRepository $googleSheetRepository)
    {
        $this->googleSheetRepository = $googleSheetRepository;
    }

    /**
     * @param Product[] $products
     * @return string
     * @throws ExceptionInterface
     */
    public function exportProducts(array $products): string
    {
        $spreadsheetId = $this->googleSheetRepository->createSpreadSheet();
        $this->googleSheetRepository->setSpreedSheetPermissions($spreadsheetId);
        $this->googleSheetRepository->saveSpreadSheetValues($spreadsheetId, $products);

        return $spreadsheetId;
    }
}
