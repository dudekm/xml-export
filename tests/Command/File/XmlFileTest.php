<?php

declare(strict_types=1);

namespace App\Tests\Command\File;

use App\Command\File\XmlFile;
use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class XmlFileTest extends KernelTestCase
{
    public function testMapping()
    {
        $encoders = [new XmlEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $xmlContent = file_get_contents('./tests/assets/coffee_feed.xml');
        $file = new XmlFile($serializer);
        $products = $file->mapping($xmlContent);

        $this->assertIsArray($products);

        /**
         * @var $product Product
         */
        $product = $products[0];

        $this->assertEquals(340, $product->getId());
        $this->assertEquals('Green Mountain Ground Coffee', $product->getCategoryName());
        $this->assertEquals(20, $product->getSku());
        $this->assertEquals(null, $product->getDescription());
        $this->assertEquals('Green Mountain Coffee French Roast Ground Coffee 24 2.2oz Bag steeps cup after cup of smoky-sweet, complex dark roast coffee from Green Mountain Ground Coffee.', $product->getShortDescription());
        $this->assertEquals(41.6000, $product->getPrice());
        $this->assertEquals('http://www.coffeeforless.com/green-mountain-coffee-french-roast-ground-coffee-24-2-2oz-bag.html', $product->getLink());
        $this->assertEquals('http://mcdn.coffeeforless.com/media/catalog/product/images/uploads/intro/frac_box.jpg', $product->getImage());
        $this->assertEquals('Green Mountain Coffee', $product->getBrand());
        $this->assertEquals(0, $product->getRating());
        $this->assertEquals('Caffeinated', $product->getCaffeineType());
        $this->assertEquals(24, $product->getCount());
        $this->assertEquals('No', $product->getFlavored());
        $this->assertEquals('No', $product->getSeasonal());
        $this->assertEquals('Yes', $product->getInstock());
        $this->assertEquals(1, $product->getFacebook());
        $this->assertEquals(0, $product->getIsKCup());
    }
}
