<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Repository\GoogleSheetRepository;
use App\Service\ExportToGoogleSheetsService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ExportToGoogleSheetsServiceTest extends KernelTestCase
{
    public function testSpreadIdIsReturned()
    {
        $googleSheetRepository = $this->createMock(GoogleSheetRepository::class);
        $googleSheetRepository->method('createSpreadSheet')
            ->willReturn('dummyId');
        $googleSheetRepository->method('setSpreedSheetPermissions');
        $googleSheetRepository->method('saveSpreadSheetValues');

        $service = new ExportToGoogleSheetsService($googleSheetRepository);

        $this->assertEquals('dummyId', $service->exportProducts([]));
    }
}
