# XML Export (to Google Sheets)

##How to run project?
1. Create a project and JSON file with credentials (https://developers.google.com/workspace/guides/create-project)
2. Make a copy of .env file, name it .env.local and set path to the credentials file (e.g. GOOGLE_APP_CREDENTIALS="credentials.json")
3. Run these commands
    ```
    docker-compose up
    docker-compose exec php composer install
    ```

#How to export XML file to Google Sheets?
Commands:
```
docker-compose exec php bin/console app:export-to-google-sheets https://domain.com/file.xml
OR
docker-compose exec php bin/console app:export-to-google-sheets C:\file.xml
```
##How to run tests?
Open your terminal and run command:
```
docker-compose exec php ./vendor/bin/phpunit
```
